<!--
<p align="center">
  <img src="https://github.com///raw/main/docs/source/logo.png" height="150">
</p>
-->

<h1 align="center">
  Detectools
</h1>


Torch overlay for trainning and inference processes for object detection & instance segmentation tasks. 

## 💪 Context

Detectools is developped by INRAE (french National Research Institute for Agriculture, Food and the Environment) for the [PHENOME-EMPHASIS project](https://www.phenome-emphasis.fr/)

## Code access and documentation

Source code  documentation will be accessible soon.